package com.example.training.groupwork.service;

import com.example.training.groupwork.entities.LolChampion;
import com.example.training.groupwork.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
// to be able to do autowiring needs to be in the container
// @Service puts it in the container
public class LolChampionService {

    @Autowired
    LolChampionRepository lolChampionRepository;

    public List<LolChampion> findAll() {
        return this.lolChampionRepository.findAll();
    }

    public LolChampion save(LolChampion lolChampion){
        return this.lolChampionRepository.save(lolChampion);
    }

}
