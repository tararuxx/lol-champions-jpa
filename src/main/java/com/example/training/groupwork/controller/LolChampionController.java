package com.example.training.groupwork.controller;

import com.example.training.groupwork.entities.LolChampion;
import com.example.training.groupwork.service.LolChampionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/lolchampion/")
public class LolChampionController {

    private static final Logger LOG = LoggerFactory.getLogger(LolChampionController.class);

    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll(){
        return this.lolChampionService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion){
        /*
        is below a good thing to do for logs? no
        should use a logger instead
         */
//        System.out.println("Request to create lolChamption [" + lolChampion + "]");
        // instead
        LOG.debug("Request to create lolChamption [" + lolChampion + "]");
        return this.lolChampionService.save(lolChampion);
    }


}
