package com.example.training.groupwork.entities;

import javax.persistence.*;

// IF DIDNT PUT IN NAMES, it would infer them
// if the table names etc dont exist
@Entity(name="lolchampion")
public class LolChampion {

    // Spring data will create the table for you
    // here you are defining the names
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ChampionID")
    private long id;

    @Column(name="name")
    private String name;

    @Column(name="role")
    private String role;

    @Column(name="difficulty")
    private String difficulty;

    @Override
    public String toString() {
        return "LolChampion{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", difficulty='" + difficulty + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }



}
