package com.example.training.groupwork.repository;

import com.example.training.groupwork.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LolChampionRepository extends JpaRepository <LolChampion, Long> {

}
